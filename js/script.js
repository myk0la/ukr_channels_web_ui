var channels = {
   "channels":[
      {
         "name":"СТБ",
         "link":"http://seetv.tv/vse-tv-online/stb-ch-ua#link=10890",
         "image":"https://www.stb.ua/wp-content/themes/STB_New/images/logo.svg"
      },
      {
         "name":"Новый канал",
         "link":"http://seetv.tv/vse-tv-online/noviukanal-tv-online#link=10891",
         "image":"https://novy.tv/wp-content/themes/novy-2018/images/logo_orange.png"
      },
      {
      	"name":"1+1",
      	"link":"https://1plus1.ua/online#online_content",
      	"image":"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/1%2B1logo.png/400px-1%2B1logo.png"
      },
      {
      	"name":"ICTV",
      	"link":"http://seetv.tv/vse-tv-online/ictv-ch-ua#link=10892",
      	"image":"https://upload.wikimedia.org/wikipedia/commons/5/55/ICTV_logo_2017.jpg"
      },
      {
      	"name":"Inter",
      	"link":"https://inter.ua/ru/live",
      	"image":"https://inter.ua/images/logo.png"
      },
      {
      	"name":"Discovery Channel",
      	"link":"http://seetv.tv/vse-tv-online/discovery-channeltv-online#link=10889",
      	"image":"https://api.discovery.com/v1/images/589b53d86b66d135760830cb?aspectRatio=original&width=105&height=&key=3020a40c2356a645b4b4"
      },
      {
      	"name":"ТРК Украина",
      	"link":"http://seetv.tv/vse-tv-online/trkukraine-tv-online#link=10964",
      	"image":"http://seetv.tv/uploads/small/b5ad0b73cf434e2fe9fa5e8db9424682.jpg"
      },
      {
      	"name":"QTV",
      	"link":"http://seetv.tv/vse-tv-online/qtv-ch-ua#link=10921",
      	"image":"http://seetv.tv/uploads/small/22c5955e9f6428139b68d1639f238707.jpg"
      },
      {
      	"name":"Discovery Science",
      	"link":"http://seetv.tv/vse-tv-online/discovery-science-tv#link=10905",
      	"image":"http://seetv.tv/uploads/small/5fee6e2e63cd08104c468e85de9e955d.jpg"
      },
      {
      	"name":"THT",
      	"link":"http://seetv.tv/vse-tv-online/tnt-tv-202#link=10897",
      	"image":"http://seetv.tv/uploads/small/540a5ca18016c89972948c556afe4b3b.jpg"
      },
      {
      	"name":"THT4 Comedy",
      	"link":"http://seetv.tv/vse-tv-online/tnt-comedy#link=10896",
      	"image":"http://seetv.tv/uploads/small/45a96e15b6f9f31f568feafbcf7d727d.jpg"
      },
      {
      	"name":"Eurosport",
      	"link":"http://seetv.tv/vse-tv-online/eurosport-1-tv#link=11044",
      	"image":"http://seetv.tv/uploads/small/e39611cab56cd33d09ebd376ae2e2351.jpg"
      },
      {
      	"name":"TV1000 Action",
      	"link":"http://seetv.tv/vse-tv-online/tv1000-action-tv#link=10912",
      	"image":"http://seetv.tv/uploads/small/f25e313083367521316f0bf59189c9d0.jpg"
      },
      {
      	"name":"VIP Megahit",
      	"link":"http://seetv.tv/vse-tv-online/vip-megahit-hd#link=11037",
      	"image":"http://seetv.tv/uploads/small/e4e8dbea1d3cfbb41e0fc1349b54899c.jpg"
      },
      {
      	"name":"Мега",
      	"link":"http://seetv.tv/vse-tv-online/mega-ch-ua#link=10908",
      	"image":"http://seetv.tv/uploads/small/0f3190458400711b5c3edb798f82f9e3.jpg"
      },
      {
      	"name":"VIP Premiere",
      	"link":"http://seetv.tv/vse-tv-online/vip-premiere-hd#link=11038",
      	"image":"http://seetv.tv/uploads/small/afe738d80aa971f91a5147bb2c86d5fe.jpg"
      },
      {
      	"name":"Первый канал",
      	"link":"http://seetv.tv/vse-tv-online/perviy-kanal#link=11023",
      	"image":"http://seetv.tv/uploads/small/5e8a2b151c7694f061733ec90cd14f68.jpg"
      },
      {
      	"name":"VIP Comedy",
      	"link":"http://seetv.tv/vse-tv-online/vip-comedy-hd#link=11039",
      	"image":"http://seetv.tv/uploads/small/2ce807a96a01f24548575698f8832f69.jpeg"
      },
      {
      	"name":"НЛО TV",
      	"link":"http://seetv.tv/vse-tv-online/nlo-tv-ch#link=10913",
      	"image":"http://seetv.tv/uploads/small/0cf4ce78e4138ea31ac30242f0c7da94.jpg"
      },
      {
      	"name":"Enter-фильм",
      	"link":"http://seetv.tv/vse-tv-online/enter-film#link=10910",
      	"image":"http://seetv.tv/uploads/small/5e87a2bbb60b7fec4a16c874eb2e1072.jpg"
      },
      {
      	"name":"Россия 1",
      	"link":"http://seetv.tv/vse-tv-online/rossiya-1-online#link=11030",
      	"image":"http://seetv.tv/uploads/small/b1c7ad994484a82bd7cbc5e1a869ebf6.png"
      },
      {
      	"name":"НТВ",
      	"link":"http://seetv.tv/vse-tv-online/ntv-online#link=11042",
      	"image":"http://seetv.tv/uploads/small/3b698bb36bb450bb9d0aca4d2c02c03f.png"
      },
      {
      	"name":"РБК ТВ",
      	"link":"http://seetv.tv/vse-tv-online/rbk-tv#link=11043",
      	"image":"http://seetv.tv/uploads/small/ecbb5e64a014f1ee301f0329a17bd4af.jpg"
      },
      {
      	"name":"Nat Geo Wild",
      	"link":"http://seetv.tv/vse-tv-online/natgeowild#link=10917",
      	"image":"http://seetv.tv/uploads/small/84507023dbc4ef64ad152f33835e76a5.jpg"
      },
      {
      	"name":"National Geographic",
      	"link":"http://seetv.tv/vse-tv-online/national-geographic-channel#link=10920",
      	"image":"http://seetv.tv/uploads/small/80685b3a02da7f5d2d6301ce833f29f7.jpg"
      },
      {
      	"name":"Россия 24",
      	"link":"https://www.youtube.com/watch?v=K59KKnIbIaM",
      	"image":"http://seetv.tv/uploads/small/b1c0fac4d7fb86f1e11b1947aea46173.jpg"
      },
      {
      	"name":"СТС",
      	"link":"http://seetv.tv/vse-tv-online/ctc#link=91",
      	"image":"http://seetv.tv/uploads/small/a4a2a93355a4e7bd1115856b2496f548.jpg"
      },
      {
      	"name":"2+2",
      	"link":"https://2plus2.ua/online#online_content",
      	"image":"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/2%2B2_logo_2017.svg/400px-2%2B2_logo_2017.svg.png"
      }
   ]
};

var inHTML = "";

$.each(channels.channels, function(index, value){
    var newItem = "<div class=\"col-lg-4 col-sm-6 text-center mb-4\"> <a href=\"" + value.link + "\"> ";
    newItem += "<img class=\"channel-image rounded-circle img-fluid d-block mx-auto\" src=\""+ value.image +"\" alt=\"stbImage\">"
    newItem += "<h3>" + value.name + "</h3>"
    newItem += "<p> Channel Description </p>"
    newItem += "</a></div>"
    inHTML += newItem;  
});

$("div#channelsList").append(inHTML);